<?php

namespace StatusCode;

// Add these import statements:
use StatusCode\Model\StatusCode;
use StatusCode\Model\StatusCodeTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    // getAutoloaderConfig() and getConfig() methods here

    // Add this method:
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'StatusCode\Model\StatusCodeTable' =>  function($sm) {
                    $tableGateway = $sm->get('StatusCodeTableGateway');
                    $table = new StatusCodeTable($tableGateway);
                    return $table;
                },
                'StatusCodeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new StatusCode());
                    return new TableGateway('statusCode', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
}