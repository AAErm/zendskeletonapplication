<?php

namespace StatusCode\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class StatusCodeController extends AbstractActionController
{
    protected $statusCodeTable;

    public function indexAction()
    {
        return new ViewModel(array(
            'statusesCode' => $this->getStatusCodeTable()->fetchAll(),
        ));
    }

    public function addAction()
    {
    }

    public function editAction()
    {
    }

    public function deleteAction()
    {
    }

    public function getStatusCodeTable()
    {
        if (!$this->statusCodeTable) {
            $sm = $this->getServiceLocator();
            $this->statusCodeTable = $sm->get('StatsCode\Model\StatsCodeTable');
        }
        return $this->statusCodeTable;
    }
}