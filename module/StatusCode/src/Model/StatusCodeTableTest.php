<?php


require 'StatusCodeTable.php';

class StatusCodeTableTests extends PHPUnit_Framework_TestCase
{
    private $statusCodeTable;

    protected function setUp()
    {
        $this->statusCodeTable = new StatusCodeTableTests();
    }

    /**
     * @dataProvider checkGetStatusCodeByIdProvider
     */
    public function getStatusCodeTest($id, $expected)
    {
       // \Mockery::mock
        $this->assertEquals($expected, $this->statusCodeTable->getStatusCode($id));
    }

    public function saveStatusCodeTest()
    {
        $result = $this->calculator->add(1, 2);
        $this->assertEquals(3, $result);
    }

    public function deleteStatusCodeTest()
    {
        $result = $this->calculator->add(1, 2);
        $this->assertEquals(3, $result);
    }

    public function checkGetStatusCodeByIdProvider()
    {
        return array(
          'id'          => '1',
          'code'        => '300',
          'description' => 'по указанному URI существует несколько вариантов предоставления ресурса по типу MIME,
                            по языку или по другим характеристикам. Сервер передаёт с сообщением список альтернатив,
                            давая возможность сделать выбор клиенту автоматически или пользователю.',
          'expected'    => array(
              'code'        => '300',
              'description' => 'по указанному URI существует несколько вариантов предоставления ресурса по типу MIME,
                            по языку или по другим характеристикам. Сервер передаёт с сообщением список альтернатив,
                            давая возможность сделать выбор клиенту автоматически или пользователю.'
          )

        );

    }

}