<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'StatusCode\Controller\StatusCode' => 'StatusCode\Controller\StatusCodeController',
        ),
    ),

    'router' => array(
        'routes' => array(
            'statuscode' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/statuscode[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'StatusCode\Controller\StatusCode',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            'statuscode' => __DIR__ . '/../view',
    ),
    ),
);