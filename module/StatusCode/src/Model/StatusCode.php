<?php

namespace StatusCode\Model;

class StatusCode
{
    public $id;
    public $code;
    public $description;

    public function exchangeArray($data)
    {
        $this->id           = (!empty($data['id'])) ? $data['id'] : null;
        $this->code         = (!empty($data['code'])) ? $data['code'] : null;
        $this->description  = (!empty($data['description'])) ? $data['description'] : null;
    }
}