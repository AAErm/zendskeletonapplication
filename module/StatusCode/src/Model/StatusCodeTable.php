<?php

namespace StatusCode\Model;

use Zend\Db\TableGateway\TableGateway;

class StatusCodeTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getStatusCode($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveStatusCode(StatusCode $statusCode)
    {
        $data = array(
            'code'          => $statusCode->code,
            'description'   => $statusCode->description,
        );

        $id = (int) $statusCode->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getStatusCode($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Нема такого');
            }
        }
    }

    public function deleteStatusCode($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}